package org.manathome.jareporter;

import org.gradle.api.Project;
import org.gradle.api.Plugin;

/**
 * jira2doc plugin: a markup generator gradle plugin.
 */
public class Jira2DocPlugin implements Plugin<Project> {
	
	@Override
    public void apply(Project project) {
    	
		project.getLogger().debug("apply jira2doc plugin.");
					    
	    project.getTasks().register(
	    		Jira2DocPluginTask.JIRA2DOC_TASK_NAME,
	    		Jira2DocPluginTask.class);
    }
    
}
