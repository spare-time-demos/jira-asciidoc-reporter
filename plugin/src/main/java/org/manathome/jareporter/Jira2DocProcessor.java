package org.manathome.jareporter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.manathome.jareporter.core.JiraIssueReader;
import org.manathome.jareporter.core.TemplateWriter;
import org.manathome.jareporter.model.JiraIssue;
import org.manathome.jareporter.model.ReleaseGroup;

public class Jira2DocProcessor {
	
	private final JiraIssueReader reader;
	private final String jiraWebBaseUrl;
	private final String projectName;
	
	public Jira2DocProcessor(
			final String jiraRestBaseUrl, 
			final String jiraWebBaseUrl,
			final String projectName
			) {
		this.reader = new JiraIssueReader(jiraRestBaseUrl);
		this.jiraWebBaseUrl = jiraWebBaseUrl;
		this.projectName = projectName;
	}
	
	public String process(final File output, boolean withHeaders) throws FileNotFoundException, IOException {

		if (output == null) {
			throw new NullPointerException("output file argument null.");
		}

		final Stream<JiraIssue> issues = retrieveClosedIssues(projectName);
		
		final List<ReleaseGroup> releases = sortAndGroup(issues);
		
		final TemplateWriter template = new TemplateWriter("templates/changelog.peb");
		
		final String markup = template.generate(
				releases, 
				withHeaders,
				this.jiraWebBaseUrl);

		try (PrintWriter out = new PrintWriter(output)) {
			out.print(markup);
		}

		return markup;
	}
	
	public Stream<JiraIssue> retrieveClosedIssues(final String projectName) throws IOException {
						
		return this.reader.retrieveClosedIssues(projectName);
	};


	public List<ReleaseGroup> sortAndGroup(Stream<JiraIssue> issues) {
		
		List<JiraIssue> issueList = issues.collect(Collectors.toList());
		
		Set<String> versionSet = new HashSet<>();
		
		List<ReleaseGroup> releases = issueList
				.stream()
				.filter(issue -> issue.fields.fixVersions != null && issue.fields.fixVersions.length > 0) // only with versions
				.filter(issue -> versionSet.add((issue.fields.fixVersions[0].name))) // add once
				.map(issue -> new ReleaseGroup(issue.fields.fixVersions[0]))
				.collect(Collectors.toList())
				;
		
		// add empty release
		if(issueList.stream().anyMatch(i -> i.fields.fixVersions == null || i.fields.fixVersions.length <= 0)) {
			ReleaseGroup emptyRelease = new ReleaseGroup();
			releases.add(emptyRelease);
		}
		
		return releases
				.stream()
				.map(release -> { 
					release.issues = issueList.stream()
							                  .filter(issue -> release.withIssue(issue))
							                  .collect(Collectors.toList());
					return release;
				})
				.collect(Collectors.toList())
				;
	}

}
