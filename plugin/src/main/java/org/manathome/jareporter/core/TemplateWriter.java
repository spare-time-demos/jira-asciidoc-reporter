package org.manathome.jareporter.core;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.manathome.jareporter.model.JiraIssue;
import org.manathome.jareporter.model.ReleaseGroup;

import com.mitchellbosecke.pebble.PebbleEngine;
import com.mitchellbosecke.pebble.template.PebbleTemplate;

/** use pebble template to generate asciidoc output from jira issues. */
public class TemplateWriter {
	
	private final PebbleTemplate template;
	
	public TemplateWriter(final String templatePath) {
		final PebbleEngine engine = new PebbleEngine.Builder().build();
		this.template = engine.getTemplate(templatePath);		
	}
	
	public String generate(List<ReleaseGroup> releaseGroups, boolean withHeaders, String jiraWebBaseUrl) throws IOException {
				
		Writer writer = new StringWriter();

		Map<String, Object> context = new HashMap<>();
		
		List<JiraIssue> issues = releaseGroups
				.stream()
				.flatMap(r -> r.issues.stream())
				.collect(Collectors.toList());
		
		context.put(TemplateFieldTags.releaseGroups, releaseGroups);
		context.put(TemplateFieldTags.issues, issues);
		context.put(TemplateFieldTags.withHeaders, withHeaders); 
		context.put(TemplateFieldTags.jiraWebBaseUrl, jiraWebBaseUrl);

		template.evaluate(writer, context);

		String output = writer.toString();		
		return output;
	}

}
