package org.manathome.jareporter.core;

import org.manathome.jareporter.model.JiraIssue;
import org.manathome.jareporter.model.ReleaseGroup;

/** available tags in changelog.peb template. */
public final class TemplateFieldTags {
	
	/** generate changelog with caption/headers. true/false. */
	public static final String withHeaders = "withHeaders";

	/** list of {@link JiraIssue}. */
	public static final String issues = "issues";
	
	/** list of {@link ReleaseGroup}. */
	public static final String releaseGroups = "releaseGroups";
	
	/** link to jira web interface. */
	public static final String jiraWebBaseUrl = "jiraWebBaseUrl";

}
