package org.manathome.jareporter.core;

import java.io.IOException;
import java.util.Arrays;
import java.util.stream.Stream;

import org.manathome.jareporter.PluginEnvironmentVariables;
import org.manathome.jareporter.model.JiraIssue;
import org.manathome.jareporter.model.QueryResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import okhttp3.Credentials;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class JiraIssueReader {
	
	private static final Logger logger = LoggerFactory.getLogger(JiraIssueReader.class);
	
	private final ObjectMapper om = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	private final MediaType mediaTypeJson = MediaType.parse("application/json");
 	
	public final String jiraRestUrl;
	
	public JiraIssueReader(final String jiraRestUrl) {
		
		if(jiraRestUrl == null ||  jiraRestUrl.isBlank()) {
			throw new RuntimeException("no JIRA REST URL given");
		}
		
		this.jiraRestUrl = jiraRestUrl;
	}
	
	public Stream<JiraIssue> retrieveClosedIssues(final String projectName) throws IOException {
		
		final OkHttpClient client = new OkHttpClient();

		final String basicAuthUser = System.getenv(PluginEnvironmentVariables.JIRA_AUTH_USER);
		if( basicAuthUser == null || basicAuthUser.length() <= 2) {
			throw new RuntimeException("no auth user found with System.getenv" + PluginEnvironmentVariables.JIRA_AUTH_USER);
		}		
		final String basicAuthToken = System.getenv(PluginEnvironmentVariables.JIRA_AUTH_TOKEN);
		if( basicAuthToken == null || basicAuthToken.length() <= 5) {
			throw new RuntimeException("no auth token found with System.getenv: " + PluginEnvironmentVariables.JIRA_AUTH_TOKEN);
		}
		if(projectName == null ||  projectName.isBlank()) {
			throw new RuntimeException("no project name given");
		}
		
		// 	"summary", "status", "labels", "fixVersions"
		
		RequestBody body = RequestBody.create(
				"{\"jql\" : \"project = " + projectName + " AND status = done\",\"fields\": [\"summary\", \"status\", \"labels\", \"fixVersions\"]}", 
				mediaTypeJson);
		Request request = new Request.Builder()
		  .url(jiraRestUrl + "/search")
		  .post(body)
		  .addHeader("content-type", "application/json")
		  .addHeader("User-Agent", "gradle/jira-asciidoc-reader/OkHttp")
		  .addHeader("Authorization", Credentials.basic(basicAuthUser, basicAuthToken))
		  .build();

		Response response = client.newCall(request).execute();
		defaultValidateOk(response);
		
		return mapResponseToIssues(response.body().string());
	}
	
	public Stream<JiraIssue> mapResponseToIssues(final String jsonResponse) {
		
		try {
			
			QueryResponse qr = om.readValue(jsonResponse, QueryResponse.class);
			return Arrays.stream(qr.issues);
			
		} catch (Exception e) {
			logger.error("error reading json", e);
			throw new RuntimeException("error reading json" + e.getMessage(), e);
		}
	}
	
	private void defaultValidateOk(final Response response) throws IOException {
		if(response == null) throw new RuntimeException("no response");
		if(!response.isSuccessful()) throw new RuntimeException("error rest call: " + response.message() + response.code());
	}
	
}
