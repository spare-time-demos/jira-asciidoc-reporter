package org.manathome.jareporter.model;

import java.util.List;

public class ReleaseGroup {
	
	public ReleaseGroup() {}
	
	public ReleaseGroup(final Version version) {
		this.version = version;
	}
	
	public Version version;
	
	public String getVersionName() {
		return version == null || version.name == null ? "no release" : version.name;
	}
	
	public List<JiraIssue> issues;

	public boolean withIssue(final JiraIssue issue) {
		
		if(issue == null) {
			return false;
		}
		
		return	
		 (this.version == null && (issue.fields.fixVersions == null || issue.fields.fixVersions.length == 0)) 
         || 
         (
         this.version != null &&
         issue.fields.fixVersions != null &&
         issue.fields.fixVersions.length > 0 &&
         issue.fields.fixVersions[0].name.equals(this.version.name)
         );	
	}

}
