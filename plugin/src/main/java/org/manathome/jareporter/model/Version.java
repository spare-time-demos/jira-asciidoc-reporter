package org.manathome.jareporter.model;

/** json fixVersion. */
public class Version {
	public String self;
	public String description;
	public String name;
	public boolean released;
}
