package org.manathome.jareporter.model;

/** jira json subset. */
public class JiraIssue {
	
	public String id;
	public String key;
	public String self;
	
	public JiraIssueFields fields;
}
