package org.manathome.jareporter.model;

import java.util.ArrayList;
import java.util.List;

/** json jira issue substructure. */
public class JiraIssueFields {
	
	public String summary;
	
	public List<String> labels = new ArrayList<String>();
	
	public Version[] fixVersions;
}
