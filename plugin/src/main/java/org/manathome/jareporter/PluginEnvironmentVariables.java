package org.manathome.jareporter;

/** environment variable used. */
public class PluginEnvironmentVariables {

	/** environment variable, jira user. */
	public static final String JIRA_AUTH_USER = "JIRA_AUTH_USER";
	
	/** jira api token. */
	public static final String JIRA_AUTH_TOKEN = "JIRA_AUTH_TOKEN";
}
