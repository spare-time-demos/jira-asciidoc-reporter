package org.manathome.jareporter;

import java.io.File;

import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.OutputFile;
import org.gradle.api.tasks.TaskAction;

/** gradle build task. */
public class Jira2DocPluginTask extends DefaultTask {

	public static final String JIRA2DOC_TASK_NAME = "jira2docGenerate";	

	private File markupFile = null;
	
	private boolean withHeaders = true;
	
	private String jiraRestBaseUrl;
	private String jiraWebBaseUrl;
	private String projectName;
	
	public String getProjectName() {
		return projectName;
	}

	/** jira project name. */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getJiraRestBaseUrl() {
		return jiraRestBaseUrl;
	}

	/** rest api base url (will be used for data retrieval). */
	public void setJiraRestBaseUrl(final String jiraRestBaseUrl) {
		this.jiraRestBaseUrl = jiraRestBaseUrl;
	}
	
	public String getJiraWebBaseUrl() {
		return jiraWebBaseUrl;
	}

	/** web url jira (will be used for linking in asciidoc). */
	public void setJiraWebBaseUrl(String jiraWebBaseUrl) {
		this.jiraWebBaseUrl = jiraWebBaseUrl;
	}

	@OutputFile
	public File getMarkupFile() {
		return markupFile;
	}

	/** output asciidoc changelog file. */
	public void setMarkupFile(File markupFile) {
		this.markupFile = markupFile;
	}
	
	public boolean isWithHeaders() {
		return withHeaders;
	}

	/** generate a level 1 caption "changelog". */
	public void setWithHeaders(boolean withHeaders) {
		this.withHeaders = withHeaders;
	}

	/** ctor. */
	public Jira2DocPluginTask() {
		super();
		setGroup("documentation");
		setDescription("Parses java source files and produce asciidoc markup.");

		this.getLogger().debug("ctor " + JIRA2DOC_TASK_NAME);
	}

	/** parse and generate. 
	 * 
	 * @throws Exception (io, authentication...)
	 * */
	@TaskAction
	public void jira2docGenerate() throws Exception {
		
		this.getLogger().info("generating changelog ..: " + this.getMarkupFile().getCanonicalFile());
		
		Jira2DocProcessor s2d = new Jira2DocProcessor(jiraRestBaseUrl, jiraWebBaseUrl, projectName);
		s2d.process(
				this.markupFile,
				withHeaders);
	}


	
}
