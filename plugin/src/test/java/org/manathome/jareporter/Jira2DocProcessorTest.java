package org.manathome.jareporter;

import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;
import java.nio.file.Files;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.manathome.jareporter.Jira2DocProcessor;
import org.manathome.jareporter.model.ReleaseGroup;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;

public class Jira2DocProcessorTest {
	
	private File output = new File("build/changelog.adoc");
	
	private WireMockServer wireMockServer;
	
    @BeforeEach
    public void beforeEach () {
        wireMockServer = new WireMockServer(options()
        		.dynamicPort()
                .usingFilesUnderDirectory("src/test/resources/json"));
        
        wireMockServer.start();

        wireMockServer.stubFor(post(WireMock.anyUrl())
                .willReturn(WireMock.aResponse().withHeader("Content-Type", "text/json")
                .withStatus(200)
                .withBodyFile("issues.json")));    
    }
 	
    @AfterEach
    public void afterEach () {
        wireMockServer.stop();
    }
	
    @Test 
    public void processJiraToMarkupFile() throws Exception {
    	
    	if(output.exists()) {
    		output.delete();
    	}
    	
    	//  = "https://man-at-home.atlassian.net/rest/api/3"
    	
    	Jira2DocProcessor p = new Jira2DocProcessor(wireMockServer.baseUrl() + "/rest/api/3", PluginTestConstants.jiraWebUrl, PluginTestConstants.projectName);
    	assertThat(wireMockServer.baseUrl().startsWith("http"));
    	
    	p.process(output, true);
    	
    	assertThat(output.exists()).isTrue();
    	String content = Files.readString(output.toPath());
    	
    	System.out.println("-------------------------");
    	System.out.println(content);
    	System.out.println("-------------------------");
    	
    	assertThat(content).contains("JAREP-");
    	assertThat(content).contains("= changelog");
    }
    
    
    @Test 
    public void processRequiresProjectNameNok() throws Exception {

    	try {
    		Jira2DocProcessor p = new Jira2DocProcessor(wireMockServer.baseUrl() + "/rest/api/3", PluginTestConstants.jiraWebUrl, "");
        	p.process(output, true);
    		fail("exception expected");
    	} catch(Exception ex) {}
    }    

    @Test 
    public void processRequiresRestUrlNok() throws Exception {

    	try {
    		new Jira2DocProcessor(" ", PluginTestConstants.jiraWebUrl, PluginTestConstants.projectName);
    		fail("exception expected without rest api");
    	} catch(Exception ex) {}
    }    
    	
	@Test
	public void testSortAndGroup() throws Exception {
		
    	Jira2DocProcessor p = new Jira2DocProcessor(wireMockServer.baseUrl() + "/rest/api/3", PluginTestConstants.jiraWebUrl, PluginTestConstants.projectName);    	
    	
 	    List<ReleaseGroup> releaseGroups = p.sortAndGroup(p.retrieveClosedIssues(PluginTestConstants.projectName));
 	    
 	    assertThat(releaseGroups.size()).as("one release, one null release").isEqualTo(2);
 	    assertThat(releaseGroups.stream().flatMap(rg -> rg.issues.stream()).count()).as("issue count").isEqualTo(PluginTestConstants.FIXTURE_ISSUE_COUNT);
 	    assertThat(releaseGroups.stream().allMatch(rg -> rg.issues.size() >= 1)).as("each release with at least 1 issue").isTrue();
	}

}
