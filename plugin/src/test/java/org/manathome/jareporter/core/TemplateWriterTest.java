package org.manathome.jareporter.core;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.manathome.jareporter.PluginTestConstants;
import org.manathome.jareporter.model.JiraIssue;
import org.manathome.jareporter.model.JiraIssueFields;
import org.manathome.jareporter.model.ReleaseGroup;
import org.manathome.jareporter.model.Version;

public class TemplateWriterTest {

	private TemplateWriter template = new TemplateWriter("templates/changelog.peb");
	
	public static List<ReleaseGroup> buildIssues(int releaseCount) {

		List<ReleaseGroup> rgs = new ArrayList<>();
		
		for(int i = 0; i < releaseCount; i++) {
			
			Version version = new Version();
			version.name = "0.1. " + (i+1) + " .junit";
			version.description = "internal test release fake only " + (i+1);
								
			List<JiraIssue> issues = new ArrayList<JiraIssue>();
			JiraIssue issue = new JiraIssue();
			issue.id = "123456" + i;
			issue.key = "JAREP-1" + i;
			issue.self = "https://jira.domain.com/link/to/issue/1";
			issue.fields = new JiraIssueFields();
			issue.fields.summary = "a short issue headline " + i;
			issue.fields.labels.add("infrastructure");
			issue.fields.labels.add("label2");
			issue.fields.fixVersions = new Version[1];
			issue.fields.fixVersions[0] = version;
		
			issues.add(issue);
			
			ReleaseGroup rg = new ReleaseGroup();
			rg.issues = issues;
			rg.version = version;
			rgs.add(rg);		
		}

		// add *no* release..
		List<JiraIssue> issues = new ArrayList<JiraIssue>();
		JiraIssue issue = new JiraIssue();
		issue.id = "007";
		issue.key = "JAREP-99998";
		issue.self = "https://jira.domain.com/link/to/issue/99998";
		issue.fields = new JiraIssueFields();
		issue.fields.summary = "a short issue without a release version";
		issue.fields.labels.add("no-release");
	
		issues.add(issue);
		ReleaseGroup rg = new ReleaseGroup();
		rg.issues = issues;
		rg.version = null;
		rgs.add(rg);				
		
		return rgs;
	}
				
	
	@Test
	public void testChangelogTemplateFilledWithIssueData() throws Exception {

		String markup = template.generate(buildIssues(1), false, PluginTestConstants.jiraWebUrl);
		
		assertThat(markup).contains("a short issue headline");
		assertThat(markup).contains("JAREP-1");
	}
	
	@Test
	public void testChangelogTemplateFilledWithNoReleaseSection() throws Exception {

		String markup = template.generate(buildIssues(2), false, PluginTestConstants.jiraWebUrl);
		
		assertThat(markup).contains("== *no release*");
	}	

	@Test
	public void testChangelogTemplateFilledWithLabels() throws Exception {

		String markup = template.generate(buildIssues(1), false, PluginTestConstants.jiraWebUrl);
		
		assertThat(markup).contains("^infrastructure^");
		assertThat(markup).contains("^label2^");
	}

	@Test
	public void testChangelogTemplateFilledWithWebLinkToIssue() throws Exception {

		String markup = template.generate(buildIssues(1), false, PluginTestConstants.jiraWebUrl);
		
		assertThat(markup).contains( PluginTestConstants.jiraWebUrl);
	}
	
	@Test
	public void testChangelogTemplateFilledWithHeader() throws Exception {

		String markup = template.generate(buildIssues(3), true, PluginTestConstants.jiraWebUrl);		
		assertThat(markup).contains("= changelog");
		
		markup = template.generate(buildIssues(2), false, PluginTestConstants.jiraWebUrl);		
		assertThat(markup).doesNotContain("= changelog");		
	}	

	
	@Test
	public void testChangelogTemplateWithoutIssues() throws Exception {

		List<ReleaseGroup> rgs = new ArrayList<>();
		
		String markup = template.generate(rgs, true, PluginTestConstants.jiraWebUrl);		
		assertThat(markup).contains("= changelog");
	}
}
