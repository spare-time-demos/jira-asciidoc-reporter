package org.manathome.jareporter.core;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.manathome.jareporter.PluginTestConstants;
import org.manathome.jareporter.core.JiraIssueReader;
import org.manathome.jareporter.model.JiraIssue;

public class JiraIssueReaderTest {
	
	private JiraIssueReader reader = new JiraIssueReader(PluginTestConstants.jiraRestBaseUrl);
	
	public static String readFromFile() throws IOException {		
		
		final Path p = Paths.get("src/test/resources/json/__files/issues.json");		
		String content = Files.readString(p);
		return content;
	}
	
	@Test
	public void testReadFromFile() throws Exception {
		String s = readFromFile();
		assertThat(s).contains("issues");
	}

	@Test
	public void testReadFromJsonResultToJiraIssue() throws Exception {
		String s = readFromFile();
		Stream<JiraIssue> issues = reader.mapResponseToIssues(s);
		List<JiraIssue> i = issues.collect(Collectors.toList());
		
		// will fail on new jira issues in json, adapt in this case:
		assertThat(i.size()).as("jira issue count").isEqualTo(PluginTestConstants.FIXTURE_ISSUE_COUNT);
		assertThat(i.get(0).key).as("first issue returned, number").isEqualTo("JAREP-11");
		assertThat(i.get(0).self).startsWith("https://");
		assertThat(i.get(0).id).isEqualTo("10015");
		assertThat(i.get(0).fields.fixVersions.length).isEqualTo(1);
		assertThat(i.get(0).fields.fixVersions[0].name).isEqualTo("v0.1.0.poc");
	}

}
