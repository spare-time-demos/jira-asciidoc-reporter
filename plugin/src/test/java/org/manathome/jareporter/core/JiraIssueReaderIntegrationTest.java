package org.manathome.jareporter.core;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.manathome.jareporter.PluginEnvironmentVariables;
import org.manathome.jareporter.PluginTestConstants;
import org.manathome.jareporter.core.JiraIssueReader;
import org.manathome.jareporter.model.JiraIssue;

/** access actual jira instance for this project, may fail whenever jira server is down. */
public class JiraIssueReaderIntegrationTest {
	
	private JiraIssueReader reader = new JiraIssueReader(PluginTestConstants.jiraRestBaseUrl);

	@Test
	public void readIssuesFromJiraCloud() throws IOException {		

		final Stream<JiraIssue> issues = reader.retrieveClosedIssues(PluginTestConstants.projectName);
		assertThat(issues).isNotNull();
	}

	@Test
	public void readAndCheckContentIssuesFromJiraCloud() throws IOException {		

		final Stream<JiraIssue> issues = reader.retrieveClosedIssues(PluginTestConstants.projectName);
		List<JiraIssue> i = issues.collect(Collectors.toList());
		assertThat(i.size()).isGreaterThanOrEqualTo(3);
	}
	
	@Test
	@Disabled("access outside this project, do not do this in ci pipeline..")
	public void readAndCheckContentIssuesFromJiraCloudForTimeAtWorkProject() throws IOException {		

		final Stream<JiraIssue> issues = reader.retrieveClosedIssues("TAW");	// access another project.
		List<JiraIssue> i = issues.collect(Collectors.toList());
		assertThat(i.size()).isGreaterThanOrEqualTo(1);
	}
		
	
	@Test
	public void checkEnvironmentVariableRequiredForThisTest() {
		final String user = System.getenv(PluginEnvironmentVariables.JIRA_AUTH_USER);
		assertThat(user).as("user").contains("@");
		
		final String token = System.getenv(PluginEnvironmentVariables.JIRA_AUTH_TOKEN);
		assertThat(token).as("token").isNotBlank();		
	}

}
