package org.manathome.jareporter;

public class PluginTestConstants {

	public static final String jiraRestBaseUrl = "https://man-at-home.atlassian.net/rest/api/3";
	public static final String jiraWebUrl  = "https://man-at-home.atlassian.net";
	public static final int FIXTURE_ISSUE_COUNT = 3;
	public static final String projectName = "JAREP";

}
