package org.manathome.jareporter;

import static org.assertj.core.api.Assertions.assertThat;  

import org.gradle.testfixtures.ProjectBuilder;
import org.gradle.api.Project;
import org.gradle.api.Task;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

/**
 * A simple unit test for plugin.
 */
@Tag("plugin")
public class Jira2DocPluginTest {
	
    @Test 
    public void pluginRegistersItsTask() {
        assertThat(
        		getTaskForTest())
        .isNotNull();
    }
    	
    @Test 
    public void pluginTaskHasOutputsProperties() {
        assertThat(getTaskForTest().getOutputs().getHasOutput()).isTrue();
    }    

    private Task getTaskForTest() {
        Project project = ProjectBuilder.builder().build();
        project.getPlugins().apply("org.manathome.jareporter");
        return project.getTasks().findByName(Jira2DocPluginTask.JIRA2DOC_TASK_NAME);
    }
}
